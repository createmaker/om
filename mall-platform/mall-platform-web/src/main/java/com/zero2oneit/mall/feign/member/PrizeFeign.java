package com.zero2oneit.mall.feign.member;

import com.zero2oneit.mall.common.bean.member.PrizeInfo;
import com.zero2oneit.mall.common.bean.member.PrizeRule;
import com.zero2oneit.mall.common.bean.member.PrizeWinMapping;
import com.zero2oneit.mall.common.query.member.*;
import com.zero2oneit.mall.common.utils.R;
import com.zero2oneit.mall.common.utils.bootstrap.BoostrapDataGrid;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Description:
 *
 * @author Cmx
 * @date 2021/5/24 11:32
 */
@FeignClient("member-service")
public interface PrizeFeign {

    @PostMapping("/admin/member/prizeRule/list")
    BoostrapDataGrid list(@RequestBody PrizeRuleQueryObject qo);

    @PostMapping("/admin/member/prizeRule/addOrEdit")
    R addOrEdit(@RequestBody PrizeRule prizeRule);

    @PostMapping("/admin/member/prizeRecord/recordList")
    BoostrapDataGrid recordList(@RequestBody PrizeRecordQueryObject qo);

    @PostMapping("/admin/member/prizeInfo/list")
    BoostrapDataGrid prizeInfoList(@RequestBody PrizeInfoQueryObject qo);

    @PostMapping("/admin/member/prizeInfo/addOrEdit")
    R addOrEdit(@RequestBody PrizeInfo prizeInfo);

    @PostMapping("/admin/member/prizeWin/winList")
    BoostrapDataGrid winList(PrizeWinQueryObject qo);

    @PostMapping("/admin/member/prizeWin/IssueStatus")
    R IssueStatus(@RequestParam("id") String id, @RequestParam("status") Integer status);

    @PostMapping("/admin/member/prizeWinMapping/list")
    BoostrapDataGrid list(@RequestBody PrizeWinMappingQueryObject qo);

    @PostMapping("/admin/member/prizeWinMapping/addOrEdit")
    R addOrEdit(@RequestBody PrizeWinMapping prizeWinMapping);

    @PostMapping("/admin/member/prizeWinMapping/delByIds")
    R delByIds(@RequestParam("ids") String ids);

    @PostMapping("/admin/member/prizeWinMapping/status")
    R status(@RequestParam("id") String id, @RequestParam("status") Integer status);

}
